
#!/bin/bash
# First install the opensource.is repositories
#rpm -ihv http://opensource.is/repo/ok-release.rpm
#yum update -y ok-release

# Redhat/Centos users need to install the epel repositories (fedora users skip this step)
#yum install -y epel-release

# Install nagios, adagios and other needed packages
yum --enablerepo=ok-testing install -y pnp4nagios mk-livestatus nagios git adagios okconfig acl

# If you don't know how to configure SElinux, put it in permissive mode:
sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/sysconfig/selinux
setenforce 0

# Now all the packages have been installed, and we need to do a little bit of
# configuration before we start doing awesome monitoring

# Lets make sure adagios can write to nagios configuration files, and that
# it is a valid git repo so we have audit trail
cd /etc/nagios/
git init
git add .
git commit -a -m "Initial commit"

# Make sure nagios group will always have write access to the configuration files:
chown -R nagios /etc/nagios/* /etc/nagios/.git
setfacl -R -m group:nagios:rwx /etc/nagios/
setfacl -R -m d:group:nagios:rwx /etc/nagios/

# By default objects created by adagios will go to /etc/nagios/adagios so make sure that this directory exists and nagios.cfg contains a reference to this directory.
mkdir -p /etc/nagios/adagios
pynag config --append cfg_dir=/etc/nagios/adagios

# The status view relies on broker modules livestatus and pnp4nagios, so lets configure
# nagios.cfg to use those
pynag config --append "broker_module=/usr/lib64/nagios/brokers/npcdmod.o config_file=/etc/pnp4nagios/npcd.cfg"
pynag config --append "broker_module=/usr/lib64/mk-livestatus/livestatus.o /var/spool/nagios/cmd/livestatus"
pynag config --set "process_performance_data=1"

# Add nagios to apache group so it has permissions to pnp4nagios's session files
usermod -G apache nagios

# We need to restart both apache and nagios so new changes take effect
service nagios restart ; chkconfig nagios on
service httpd restart ; chkconfig httpd on
service npcd restart ; chkconfig npcd on
