el7
===

Packages that are not readily available in epel or elrepo for el7
Pretty much rebuilt packages from older (el6) or newer (fedora 19/20) src.rpms that I run daily on my own systems.
If you break your systems using these, you get to keep the broken pieces,
I take NO responsibility.

Chromium is rebuilt using RussianFedoras builds and grabbed from github:
https://github.com/RussianFedora/chromium/tree/stable
https://github.com/RussianFedora/chromium-pepper-flash

some or all of these packages assume you have these repos enabled:
epel + elrepo + nux-desktop.
