%if 0%{?fedora} > 12
%global with_python3 1
%else
%{!?python_sitelib: %global python_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib())")}
%endif
%{!?__python2:%global __python2 %{__python}}
%{!?python2_sitelib:%global python2_sitelib %{python_sitelib}}

%global         pkgname Django

# Tests requiring Internet connections are disabled by default
# pass --with internet to run them (e.g. when doing a local rebuild
# for sanity checks before committing)
%bcond_with internet

# one higher than the last Django release, to account for
# dist tags

Name:           python-django15
Version:        1.5.6
Release:        1%{?dist}
Summary:        A high-level Python Web framework

Group:          Development/Languages
License:        BSD
URL:            http://www.djangoproject.com/
Source0:        http://pypi.python.org/packages/source/D/Django/Django-%{version}.tar.gz

Source1:        python-django15-README.fedora

# patch tests to skip tests requiring internet connection
Patch0:         python-django-1.5-fix-tests.patch

Patch99:        django1.5-parallel-version.patch

BuildArch:      noarch
BuildRequires:  python-setuptools
BuildRequires:  python2-devel
%if 0%{?rhel} == 6 
BuildRequires:  python-sphinx10
%else
BuildRequires:  python-sphinx
%endif

%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif

# Require setuptools as the consumer will need pkg_resources to use this module
# to make a parallel installable version
Requires:       python-setuptools
Requires:       bash-completion


%description
Django is a high-level Python Web framework that encourages rapid
development and a clean, pragmatic design. It focuses on automating as
much as possible and adhering to the DRY (Don't Repeat Yourself)
principle.

%package doc
Summary:        Documentation for Django
Group:          Documentation
Requires:       %{name} = %{version}-%{release}

%description doc
This package contains the documentation for the Django high-level
Python Web framework.

%if 0%{?with_python3}
%package -n python3-django15-doc
Summary:        Documentation for Django
Group:          Documentation
Requires:       python3-django15 = %{version}-%{release}

%description -n python3-django15-doc
This package contains the documentation for the Django high-level
Python Web framework.

%package -n python3-django15
Summary:        A high-level Python Web framework
Group:          Development/Languages

Requires:       python3
Requires:       bash-completion

%description -n python3-django15
Django is a high-level Python Web framework that encourages rapid
development and a clean, pragmatic design. It focuses on automating as
much as possible and adhering to the DRY (Don't Repeat Yourself)
principle.
%endif

%prep
%setup -q -n %{pkgname}-%{version}
%patch0

%patch99 -p1

# place a readme file on how to use this package
cp %SOURCE1 README.fedora

# empty files
for f in \
    django/contrib/markup/models.py \
    django/contrib/staticfiles/models.py \
    django/contrib/webdesign/models.py \
    django/contrib/humanize/models.py \
; do
  echo "# just a comment" > $f
done
echo "<!-- nothing -->" > django/contrib/flatpages/tests/templates/registration/login.html

# copy jQuery license file to docs
cp django/contrib/admin/static/admin/js/LICENSE-JQUERY.txt .

# remove '/usr/bin/env python' from scripts in python_sitelib
sed -i '1d' django/bin/profiling/gather_profile_stats.py

%if 0%{?with_python3}
cp -a . %{py3dir}
sed -i '1d' %{py3dir}/django/contrib/admin/bin/compress.py
%endif


%build
%{__python2} setup.py bdist_egg

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py bdist_egg
popd
%endif # with_python3


%install
# must do install of python 3 subpackage first, so that we don't
# overwrite django-admin script with the python 3 version
%if 0%{?with_python3}
pushd %{py3dir}
mkdir -p %{buildroot}%{python3_sitelib}
easy_install-3.3 -m --prefix %{buildroot}%{_usr} -Z dist/*.egg
mv %{buildroot}%{_bindir}/django-admin.py %{buildroot}%{_bindir}/python3-django1.5-admin
find %{buildroot}%{python3_sitelib} \
-name bin -prune \
-o -name scripts -prune \
-o -name manage.py -prune \
-o -name "*.py" -exec chmod ugo-x {} \;

popd
%endif # with_python3

mkdir -p %{buildroot}%{python_sitelib}
easy_install -m --prefix %{buildroot}%{_usr} -Z dist/*.egg
find %{buildroot}%{python2_sitelib} \
-name bin -prune \
-o -name scripts -prune \
-o -name manage.py -prune \
-o -name "*.py" -exec chmod ugo-x {} \;

%if 0%{?rhel} == 6
# Handling locale files
# This is adapted from the %%find_lang macro, which cannot be directly
# used since Django locale files are not located in %%{_datadir}
#
# The rest of the packaging guideline still apply -- do not list
# locale files by hand!
(cd $RPM_BUILD_ROOT && find . -name 'django*.mo') | %{__sed} -e 's|^.||' |
%{__sed} -e \
   's:\(.*/locale/\)\([^/_]\+\)\(.*\.mo$\):%lang(\2) \1\2\3:' \
       >> django.lang
%else
%find_lang django
%find_lang djangojs
# append djangojs.lang to django.lang
cat djangojs.lang >> django.lang
%endif
%if 0%{?with_python3}
# When creating Python3 package, separate lang to Python 2 and Python 3 files
grep py3.3 django.lang > python3-django.lang
grep py2.7 django.lang > python2-django.lang
mv {python2-,}django.lang
%endif # with_python3

# build documentation
%if 0%{?rhel} == 6
export SPHINXBUILD=sphinx-1.0-build
%endif
(cd docs && mkdir djangohtml && mkdir -p _build/{doctrees,html} && make html)


# install man pages
mkdir -p %{buildroot}%{_mandir}/man1/
cp -p docs/man/* %{buildroot}%{_mandir}/man1/
%if 0%{?with_python3}
cp -a %{buildroot}%{_mandir}/man1/django-admin.1 %{buildroot}%{_mandir}/man1/python3-django1.5-admin.1
cp -a %{buildroot}%{_mandir}/man1/daily_cleanup.1 %{buildroot}%{_mandir}/man1/python3-django1.5-daily_cleanup.1
cp -a %{buildroot}%{_mandir}/man1/gather_profile_stats.1 %{buildroot}%{_mandir}/man1/python3-django1.5-gather_profile_stats.1
%endif # with_python3

# rename man page to not conflict with main package
mv %{buildroot}%{_mandir}/man1/django-admin.1 %{buildroot}%{_mandir}/man1/django1.5-admin.1
mv %{buildroot}%{_mandir}/man1/daily_cleanup.1 %{buildroot}%{_mandir}/man1/django1.5-daily_cleanup.1
mv %{buildroot}%{_mandir}/man1/gather_profile_stats.1 %{buildroot}%{_mandir}/man1/django1.5-gather_profile_stats.1

# Fix items in %%{_bindir}, rename to avoid conflicts with main package
mv %{buildroot}%{_bindir}/django-admin.py %{buildroot}%{_bindir}/django1.5-admin

# install bash completion script
mkdir -p %{buildroot}%{_sysconfdir}/bash_completion.d/
install -m 0644 -p extras/django_bash_completion \
   %{buildroot}%{_sysconfdir}/bash_completion.d/


# remove .po files
find %{buildroot} -name "*.po" | xargs rm -f

%check
export PYTHONPATH=$(pwd)
export LANG=en_US.utf8
cd tests
./runtests.py --settings=test_sqlite --verbosity=2

%if 0%{?with_python3}
pushd %{py3dir}
export PYTHONPATH=%{py3dir}
cd tests
%{__python3} runtests.py --settings=test_sqlite --verbosity=2
popd
%endif # with_python3


%files -f django.lang 
%doc AUTHORS LICENSE README.rst README.fedora LICENSE-JQUERY.txt
# manual pages are owned by both python2 and python3 packages
%{_mandir}/man1/*
# except the symlink with python3 prefix
%if 0%{?with_python3}
%exclude %{_mandir}/man1/python3-*
%endif # with_python3
%{_bindir}/django1.5-admin
%dir %{_sysconfdir}/bash_completion.d/
%config(noreplace) %{_sysconfdir}/bash_completion.d/django_bash_completion
# Include everything but the locale data ...

%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/EGG-INFO
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/bin
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/db/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/dispatch
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/shortcuts
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/template
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/utils
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/views
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/udm/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/*/LC_MESSAGES
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/udm
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/bin
%attr(0755,root,root) %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/bin/compress.py
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/bin/compress.py?
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/views/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/static/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/templatetags/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/templates/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/udm
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/templates/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/tests/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/fixtures/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/handlers/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/management/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/templates/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/tests/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/views
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/templatetags
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/templates
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/plugins
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/templates
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/udm/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/fixtures/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/templatetags
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/tests
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/templates/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/tests/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/wizard/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/geoip/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/templatetags/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??_/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??/LC_MESSAGES
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??_*/LC_MESSAGES
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??_*/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??_/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/generic
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/markup
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/tests
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/*.py*
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/management/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sitemaps/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/??/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/??_*/
%dir %{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/*/LC_MESSAGES
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/staticfiles/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/syndication/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/webdesign/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/admin/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/db/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/forms/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/gdal/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/geometry/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/geos/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/management/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/maps/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/sitemaps/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/templates/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/tests/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/utils/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/storage/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/backends/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/forms/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/templatetags/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/core/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/http/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/middleware/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/test/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/project_template/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/app_template/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/urls/
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/*/*.py*
%{python2_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/*.py*



%files doc
%doc docs/_build/html/*

%if 0%{?with_python3}
%files -n python3-django15-doc
%doc docs/_build/html/*

%files -n python3-django15 -f python3-django.lang
%doc AUTHORS LICENSE README.rst README.fedora LICENSE-JQUERY.txt
%{_bindir}/python3-django1.5-admin
%{_mandir}/man1/python3-django1.5-daily_cleanup.1.*
%{_mandir}/man1/python3-django1.5-admin.1.*
%{_mandir}/man1/python3-django1.5-gather_profile_stats.1.*
%config(noreplace) %{_sysconfdir}/bash_completion.d/django_bash_completion

%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/EGG-INFO
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/bin
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/db/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/dispatch
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/shortcuts
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/template
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/utils
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/views
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/udm/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/??/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/??_*/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/??_*/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/*/LC_MESSAGES
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/udm
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/bin
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/views/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/static/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/templatetags/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admin/templates/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/udm/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/templates/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/admindocs/tests/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/fixtures/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/handlers/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/management/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/templates/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/auth/tests/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/views
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/templatetags
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/comments/templates
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/contenttypes/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/plugins
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/templates
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/databrowse/*.py*
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/udm
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/__pycache__/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/fixtures/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/templatetags
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/flatpages/tests
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/templates/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/tests/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/formtools/wizard/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/geoip/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/templatetags/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/humanize/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??/LC_MESSAGES
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/locale/??_*/LC_MESSAGES
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??_/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??_/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??_/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/??/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/localflavor/generic
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/markup
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/tests
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/redirects/__pycache__
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/management/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sitemaps/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/??/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/??_*/
%dir %{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/locale/*/LC_MESSAGES
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sites/__pycache__
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/staticfiles/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/syndication/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/webdesign/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/__pycache__/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/admin/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/db/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/forms/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/gdal/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/geometry/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/geos/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/management/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/maps/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/sitemaps/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/templates/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/tests/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/gis/utils/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/messages/storage/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/contrib/sessions/backends/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/forms/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/templatetags/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/core/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/http/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/middleware/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/test/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/project_template/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/app_template/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/urls/
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/*/*.py*
%{python3_sitelib}/%{pkgname}-%{version}-py?.?.egg/django/conf/locale/*.py*

%endif


%changelog
* Tue Apr 22 2014 Matthias Runge <mrunge@redhat.com> - 1.5.6-1
- update to 1.5.6 fixing CVE-2014-0473 and CVE-2014-0474

* Mon Mar 17 2014 Matthias Runge <mrunge@redhat.com> - 1.5.5-7
- fix script-without-shebang errors

* Mon Mar 10 2014 Matthias Runge <mrunge@redhat.com> - 1.5.5-6
- own more files and dirs 
- don't own /etc/bash_completion.d (it's owned by bash-completion)

* Thu Mar 06 2014 Matthias Runge <mrunge@redhat.com> - 1.5.5-5
- remove obsoletes/provides
- own python3_sitelib/../django/contrib/localflavor/locale/??/
- own python3_sitelib/../django/contrib/localflavor/locale/??/LC_MESSAGES
- fix docs build on rawhide

* Wed Mar 05 2014 Matthias Runge <mrunge@redhat.com> - 1.5.5-4
- own /usr/lib/python3.3/site-packages/Django-1.5.5-py3.3.egg
- added README.fedora
- removed eggs in %%prep
- removed executable flags from python files
- used __python2 instead of __python

* Tue Feb 25 2014 Matthias Runge <mrunge@redhat.com> - 1.5.5-3
- split out python-django15 package (new package)
- make installable in parallel

* Tue Oct 29 2013 Matthias Runge <mrunge@redhat.com> - 1.5.5-2
- fix obsoletes

* Mon Oct 28 2013 Matthias Runge <mrunge@redhat.com> - 1.5.5-1
- update to 1.5.5 (rhbz#1023407)

* Mon Sep 16 2013 Matthias Runge <mrunge@redhat.com> - 1.5.4-1
- fix CVE-2013-1443 (DoS via large passwords), fixes rhbz#1008281

* Wed Sep 11 2013 Matthias Runge <mrunge@redhat.com> - 1.5.3-1
- fix CVE-2013-4315 (ssi issue), fixes rhbz 1004969

* Tue Aug 20 2013 Matthias Runge <mrunge@redhat.com> - 1.5.2-2
- increase obsoletes to 1.4.6-2

* Wed Aug 14 2013 Matthias Runge <mrunge@redhat.com> - 1.5.2-1
- security related update

* Mon Aug 05 2013 Matthias Runge <mrunge@redhat.com> - 1.5.1-3
- rebuild to fix build issues during mass rebuild.

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Tue Apr 02 2013 Matthias Runge <mrunge@redhat.com> - 1.5.1-1
- update to Django-1.5.1 (rhbz#929413)

* Wed Mar 27 2013 Miro Hrončok <mhroncok@redhat.com> - 1.5-3
- separated lang files for Python 2 and 3
- list %%files in Python 3 subpackage more precisely to not include locale files
- added %%doc to Python 3 subpackage
- removed trailing slash after %%{buildroot} to avoid double slash
- fixed bogus dates in %%changelog
- made %%check verbose, so it is not so boring to watch them run
- created python3 doc package
- created python3- prefixed symlink to manpage
- comment added about shared content - manpages

* Wed Mar 27 2013 Bohuslav Kabrda <bkabrda@redhat.com> - 1.5-2
- Make the python3 subpackage install first, so that django-admin
  doesn't get overwritten.
- Don't own %%{python3_sitelib} itself, but only its contents.

* Wed Feb 27 2013 Matthias Runge <mrunge@redhat.com> - 1.5-1
- update to Django-1.5

* Thu Feb 21 2013 Matthias Runge <mrunge@redhat.com> - 1.4.5-2
- update to latest stable upstream version
- fix minor packaging issues introduced upstream

* Wed Feb 20 2013 Matthias Runge <mrunge@redhat.com> - 1.4.4-1
- update to 1.4.4 (rhbz #913024)
- Data leakage via admin history log CVE-2013-0305 (rhbz #913041)
- Formset denial-of-service CVE-2013-0306 (rhbz #913042)

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jan 24 2013 Matthias Runge <mrunge@redhat.com> - 1.4.3-2
- also obsolete version 1.4.3-1

* Sun Jan 13 2013 Matthias Runge <mrunge@redhat.com> - 1.4.3-2
- own directories (rhbz#894533)

* Tue Dec 11 2012 Matthias Runge <mrunge@redhat.com> - 1.4.3-1
- security update to upstream version 1.4.3
  https://www.djangoproject.com/weblog/2012/dec/10/security/

* Fri Nov 16 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 1.4.2-2
- Bump obsoletes version, since there is still Django-1.4.2-1 out there.
- Fix a provide that should allow users to use "django" instead of "Django".

* Thu Oct 18 2012 Matthias Runge <mrunge@redhat.com> - 1.4.2-1
- security update to upstream version 1.4.2-1
  https://www.djangoproject.com/weblog/2012/oct/17/security/

* Thu Sep 20 2012 Matthias Runge <mrunge@redhat.com> - 1.4.1-2
- fix provides: Django

* Wed Aug 01 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.4.1-1
- update to 1.4.1 fixing CVE-2012-3442, CVE-2012-3443, and CVE-2012-3444

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed May 30 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.4-4
- fix for FTBFS using python 2.7.3 (thanks to  M.Cepl)

* Tue Mar 27 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.4-3
- revert change from 1.4-2
- really fix simplejson-import

* Tue Mar 27 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.4-2
- fix simplejson-import (port from django-1.3-package)

* Sun Mar 25 2012 Michel Salim <salimma@fedoraproject.org> - 1.4-1
- Update to 1.4

* Mon Mar 19 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.3.1-9
- spec cleanup

* Sat Mar 17 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.3.1-8
- patch tests to work on koji (no internet connection)

* Sat Mar 10 2012 Michel Salim <salimma@fedoraproject.org> - 1.3.1-7
- Enable tests
- Now obsoletes (last Django release+1)
- Mark Bash completion script as a configuration file

* Fri Mar 09 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.3.1-6
- add additional provides django = %%{version}-%%{release}

* Wed Mar 07 2012 Matthias Runge <mrunge@matthias-runge.de> - 1.3.1-5
- rename package to python-django

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Oct 12 2011 Michel Salim <salimma@fedoraproject.org> - 1.3.1-3
- Package bash completion script

* Sat Sep 10 2011 Michel Salim <salimma@fedoraproject.org> - 1.3.1-2
- Switch to the 'html' doc builder, for easier navigation without a web server

* Sat Sep 10 2011 Michel Salim <salimma@fedoraproject.org> - 1.3.1-1
- Update to 1.3.1
- Remove workaround for non-functional -doc generation
- Deduplicate file listing

* Wed Mar 30 2011 Steve Milner <me@stevemilner.org> - 1.3-2
- Fix for BZ#693865

* Wed Mar 30 2011 Steve Milner <me@stevemilner.org> - 1.3-1
- Fix for es_MX upstream bug
- Update for upstream release

* Wed Feb  9 2011 Steve Milner <me@stevemilner.org> - 1.2.5-1
- Fix for CVE-2011-0697

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Jan  3 2011 Steve 'Ashcrow' Milner <me@stevemilner.org> - 1.2.4-1
- Update for multiple security issues (see http://www.djangoproject.com/weblog/2010/dec/22/security/)

* Sat Oct  9 2010 Steve 'Ashcrow' Milner <me@stevemilner.org> - 1.2.3-3
- Now build docs for F12+
- Added Django-remove-djangodocs-ext.patch

* Sat Oct  9 2010 Steve 'Ashcrow' Milner <me@stevemilner.org> - 1.2.3-2
- Moved to dirhtml for documentation generation

* Mon Sep 13 2010 Steve 'Ashcrow' Milner <me@stevemilner.org> - 1.2.3-1
- Update for http://www.djangoproject.com/weblog/2010/sep/10/123/

* Thu Sep  9 2010 Steve 'Ashcrow' Milner <me@stevemilner.org> - 1.2.2-1
- Update for CVE-2010-3082 (see http://www.djangoproject.com/weblog/2010/sep/08/security-release/)
- Removed Django-hash-compat-13310.patch as it is already included in this release

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 1.2.1-6
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Tue Jun  8 2010 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.2.1-5
- Added http://code.djangoproject.com/changeset/13310?format=diff&new=13310 per BZ#601212

* Thu Jun  3 2010 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.2.1-4
- Include egg in >= rhel6

* Thu Jun  3 2010 Michel Salim <salimma@fedoraproject.org> - 1.2.1-3
- Use generated %%{name}.lang instead of including each locale file by hand
- Temporarily make main package provide -doc on Rawhide, to fix upgrade path
  until upstream documentation builds with Sphinx 1.0

* Thu May 27 2010 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.2.1-2
- Allow for building docs in F13 as it's only F14 freaking out

* Tue May 25 2010 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.2.1-1
- Update for new release.
- Added lang files per BZ#584866.
- Changed perms on %%{python_sitelib}/django/contrib/admin/media/js/compress.py
- Lots of explicit files listed in %%files in order to reduce duplicate file listings
- Docs are not built on F-13 for now

* Wed Oct 21 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.1.1-2
- Removed po files per BZ#529188.

* Fri Oct  9 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.1.1-1
- Update to fix http://www.djangoproject.com/weblog/2009/oct/09/security/
- Django-ignore-pyo-bz-495046.patch no longer needed.

* Wed Aug 26 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.1-4
- EL-4 shouldn't get the sphinx docs.

* Wed Aug 26 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.1-3
- ghosting admin py* is now FC9 and under.

* Thu Aug  6 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.1-2
- Applied Daniel Mach's patch from bz#516016.

* Sat Aug  1 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.1-1
- Update for Django 1.1 release.
- Moved /usr/bin/django-admin.py to /usr/bin/django-admin
- sed macro is now being used
- Patch for bz#495046 applied.

* Wed Jul 29 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.0.3-6
- Attempted combined spec for F12/11/10 and EL5

* Wed Jul 29 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.0.3-4
- Older builds must ghost django-admin.py[c,o]

* Wed Jul 29 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.0.3-3
- Bump for tag issue.

* Wed Jul 29 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.0.3-2
- Fix changelog.

* Wed Jul 29 2009 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.0.3-1
- Upgrade for http://www.djangoproject.com/weblog/2009/jul/28/security/

* Thu Mar 12 2009 Michel Salim <salimma@fedoraproject.org> - 1.0.2-3
- Build HTML documentation (bug #484070)
- No longer excluding *.py? in bindir, F11's Python does not optimizes these

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Dec 14 2008 Michel Salim <salimma@fedoraproject.org> - 1.0.2-1
- Update to 1.0.2

* Sat Nov  1 2008 Steve 'Ashcrow' Milner <stevem@gnulinux.net> - 1.0.1-0.1.beta1
- Update to 1.0.1_beta_1

* Sat Sep  6 2008 Michel Salim <salimma@fedoraproject.org> - 1.0-1
- Update to final 1.0 release

* Tue Sep  2 2008 Michel Salim <salimma@fedoraproject.org> - 1.0-0.1.rc1%{?dist}
- CSRF security update: bz#460966

* Wed Aug 27 2008 Michel Salim <salimma@fedoraproject.org> - 1.0-0.1.beta2
- Update to 1.0 beta2

* Sat Aug 23 2008 Michel Salim <salimma@fedoraproject.org> - 1.0-0.1.beta1
- Update to 1.0 beta1

* Mon May 19 2008 Michel Salim <salimma@fedoraproject.org> - 0.96.2-1
- XSS security update: CVE-2008-2302 (bz# 442757-60)

* Sat Apr  5 2008 Michel Salim <salimma@fedoraproject.org> - 0.96.1-2
- Package .egg-info file on Fedora >= 9

* Thu Nov  1 2007 Michel Salim <michel.sylvan@gmail.com> 0.96.1-1
- i18n security update: CVE-2007-5712, bz#357051

* Sat Mar 24 2007 Michel Salim <michel.salim@gmail.com> - 0.96-1
- New upstream version

* Sun Jan 21 2007 Michel Salim <michel.salim@gmail.com> - 0.95.1-1
- Upstream security updates:
  http://www.djangoproject.com/weblog/2007/jan/21/0951/

* Sun Nov 12 2006 Michel Salim <michel.salim@gmail.com> - 0.95-1
- Initial package
