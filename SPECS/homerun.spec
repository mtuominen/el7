Name:           homerun
Version:        1.2.5
Release:        1%{?dist}
# KDE e.V. may determine that future GPL versions are accepted
License: GPLv2 or GPLv3 
Summary:        KDE Application Launcher
URL:            http://userbase.kde.org/Homerun
Source0:        http://download.kde.org/stable/homerun/src/%{name}-%{version}.tar.xz

BuildRequires:  kdelibs4-devel
BuildRequires:  kde-workspace-devel
BuildRequires:  gettext
BuildRequires:  kde-baseapps-devel

Requires: kde-runtime%{?_kde4_version: >= %{_kde4_version}}
Requires: %{name}-libs%{?_isa} = %{version}-%{release}

%description
Homerun is an alternative modern launcher for KDE.

%package libs
# KDE e.V. may determine that future LGPL versions are accepted
License: LGPLv2 or LGPLv3
Summary: Library files of homerun launcher

%description libs
This package provides the library files of the homerun launcher.


%package devel
# KDE e.V. may determine that future LGPL versions are accepted
License: LGPLv2 or LGPLv3
Summary: Development files for homerun
Requires: %{name}-libs%{?_isa} = %{version}-%{release}

%description devel
The homerun-devel package contains all the development files 
of the homerun launcher.
 
%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} .. 
popd
make %{?_smp_mflags} -C %{_target_platform}
 
%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}
%find_lang %{name} --with-kde --all-name

%post
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

 
%files -f %{name}.lang
%{_kde4_bindir}/homerunviewer
%{_kde4_appsdir}/plasma/plasmoids/org.kde.homerun/
%{_kde4_appsdir}/plasma/plasmoids/org.kde.homerun-kicker/
%{_kde4_appsdir}/homerun/
%{_kde4_libdir}/kde4/homerun_source_recentdocuments.so
%{_kde4_libdir}/kde4/plasma_applet_homerunlauncher.so
%{_kde4_libdir}/kde4/imports/org/kde/homerun/
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_datadir}/kde4/services/homerun-source-recentdocuments.desktop
%{_kde4_datadir}/kde4/services/homerunviewer.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-homerun.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-homerunlauncher.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-homerun-kicker.desktop
%{_kde4_datadir}/kde4/servicetypes/homerun-source.desktop
%{_kde4_configdir}/homerunrc 
%{_kde4_configdir}/homerunkickerrc

%files libs
%{_kde4_libdir}/libhomerun.so.0*

%files devel
%{_kde4_libdir}/libhomerun.so
%{_includedir}/homerun
%{_kde4_libdir}/cmake/Homerun


%changelog
* Mon Jun 30 2014 Rex Dieter <rdieter@fedoraproject.org> 1.2.5-1
- 1.2.5

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon May 05 2014 Joseph Marrero <jmarrero@fedoraproject.org> 1.2.4-1
- Update to latest upstream version 1.2.4

* Tue Apr 08 2014 Rex Dieter <rdieter@fedoraproject.org> 1.2.3-1
- 1.2.3

* Mon Mar 24 2014 Rex Dieter <rdieter@fedoraproject.org> 1.2.2-1
- 1.2.2

* Mon Jan 27 2014 Rex Dieter <rdieter@fedoraproject.org> 1.2.0-1
- 1.2.0

* Thu Jan 02 2014 Joseph Marrero <jmarrero@fedoraproject.org> 1.1.0-1
- update to latest upstream version
* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Jul 03 2013 Joseph Marrero <jmarrero@fedoraproject.org> 1.0.0-5
- remove /sbin/ldconfig from post and postun of the main package 
* Wed Jul 03 2013 Joseph Marrero <jmarrero@fedoraproject.org> 1.0.0-4
- Add libs requirements to main and devel packages
- Fix devel description
* Wed Jun 26 2013 Joseph Marrero <jmarrero@fedoraproject.org> 1.0.0-3
- Fix licenses
* Wed Jun 26 2013 Joseph Marrero <jmarrero@fedoraproject.org> 1.0.0-2
- Add licenses to the licence tag
- Fix descriptions
- Use kde4_appsdir macro
* Thu May 23 2013 Joseph Marrero <jmarrero@fedoraproject.org> 1.0.0-1
- Update to 1.0.0 version
* Tue Apr 30 2013 Joseph Marrero <jmarrero@fedoraproject.org> 0.2.2-4
- add libs package
* Tue Apr 30 2013 Joseph Marrero <jmarrero@fedoraproject.org> 0.2.2-3
- Fix gettext dependency and plasma folder ownership
* Fri Apr 26 2013 Joseph Marrero <jmarrero@fedoraproject.org> 0.2.2-2
- Fix files and macros
- postun and post ldconfig scriplets fix
- add gettext dep
* Thu Apr 25 2013 Joseph Marrero <jmarrero@fedoraproject.org> 0.2.2-1
- Initial packaging
